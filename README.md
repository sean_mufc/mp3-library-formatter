# README #

### What is this repository for? ###

Command line tool that will set the Artist, track & album tags on mp3's based on a predefined folder structure, which is:

- Artist 1 (folder)
    - Album 1 (folder)
        - Track_1.mp3
        - Track_2.mp3
- Artist 2
    - Album 1
        - Track_1.mp3
        - Track_2.mp3
    - Album 2
        - Track_1.mp3
        - Track_2.mp3


If a file name includes " - " anywhere, it will be split and the left part will be set as the artist name and the right part the track name (e.g: "Artist Name - Track Name.mp3")


It takes 1 command line argument - which is the root folder for all music albums.

### How do I get set up? ###

Just compile and run from the command line.