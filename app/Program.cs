﻿using System;

namespace Mp3LibraryFormatter
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Console.WriteLine("Enter diretory...");
				string libraryRootDirectory = Console.ReadLine();
				TagLibProcessor.ProcessLibraryUsingMp3Files(libraryRootDirectory);
				Console.WriteLine("DONE");
				
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			finally
			{
				Console.Read();
			}
		}
	}
}
