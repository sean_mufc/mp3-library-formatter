﻿using System;
using System.Collections.Generic;

namespace Mp3LibraryFormatter
{
	public static class TagLibManager
	{
		public static string[] GetAllCurrentFolderNames(string directory)
		{
			return System.IO.Directory.GetDirectories(directory);
		}

		public static string ExtractNameFromPath(string directory)
		{
			string[] pathParts = directory.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
			return pathParts[pathParts.Length - 1];
		}

		public static Dictionary<string, TagLib.File> GetAllCurrentMp3SFiles(string directory)
		{
			Dictionary<string, TagLib.File> mp3FileDictionary = new Dictionary<string, TagLib.File>();

			string[] allFileNames = System.IO.Directory.GetFiles(directory);

			foreach (string fileName in allFileNames)
			{
				bool isMp3File = fileName.ToLower().EndsWith(".mp3");
				if (!isMp3File)
				{
					continue;
				}

				try {

					TagLib.File mp3File = TagLib.File.Create(fileName);
					mp3FileDictionary.Add(fileName, mp3File);
				}
				catch(Exception ex)
				{
					Console.WriteLine("Error trying to create mp3File from '{0}'", fileName);
					Console.WriteLine(ex.ToString());
				}
			}

			return mp3FileDictionary;
		}

		public static bool IsEmptyTag(TagLib.Tag tag)
		{
			bool tagHasData = tag != null &&
				(
					(tag.Album != null && !string.IsNullOrWhiteSpace(tag.Album)) ||
					(tag.Title != null && !string.IsNullOrWhiteSpace(tag.Title)) ||
					(tag.Year != null)
				);

			if (tagHasData)
			{
				return false;
			}

			if (tag.AlbumArtists == null || tag.AlbumArtists.Length == 0)
			{
				return false;
			}

			return tagHasData == false;
		}

		public static TagLib.Tag UpdateTag(TagLib.Tag existingTag, string artist, string title, string album, uint track, uint year)
		{
			List<string> listArtists = new List<string>();
			listArtists.Add(artist);
			existingTag.AlbumArtists = listArtists.ToArray();
			existingTag.Artists = listArtists.ToArray();

			existingTag.Title = title;
			existingTag.Album = album;
			existingTag.Track = track;
			existingTag.Year = year;
			return existingTag;
		}
	}
}
