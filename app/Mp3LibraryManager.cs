﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Id3;
using Id3.Frames;

namespace Mp3LibraryFormatter
{
    public static class Mp3LibraryManager
    {
        public static string[] GetAllCurrentFolderNames(string directory)
        {
            return Directory.GetDirectories(directory);
        }

        public static string ExtractNameFromPath(string directory)
        {
            string[] pathParts = directory.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            return pathParts[pathParts.Length - 1];
        }

        public static Dictionary<string, Mp3File> GetAllCurrentMp3SFiles(string directory)
        {
            Dictionary<string, Mp3File> mp3FileDictionary = new Dictionary<string, Mp3File>();

            string[] allFileNames = Directory.GetFiles(directory);

            foreach (string fileName in allFileNames)
            {
                bool isMp3File = fileName.ToLower().EndsWith(".mp3");
                if (!isMp3File)
                {
                    continue;
                }

                Mp3File mp3File = new Mp3File(fileName, Mp3Permissions.ReadWrite);
                mp3FileDictionary.Add(fileName, mp3File);
            }

            return mp3FileDictionary;
        }

        public static IList<Mp3Stream> GetAllCurrentMp3SFilesAsStreams(string directory)
        {
            IList<Mp3Stream> mp3Streams = new List<Mp3Stream>();

            string[] allFileNames = Directory.GetFiles(directory);

            foreach (string fileName in allFileNames)
            {
                bool isMp3File = fileName.ToLower().EndsWith(".mp3");
                if (!isMp3File)
                {
                    continue;
                }

                FileStream mp3FileStream = File.OpenRead(fileName);
                Mp3Stream mp3Stream = new Mp3Stream(mp3FileStream, Mp3Permissions.ReadWrite);
                mp3Streams.Add(mp3Stream);
            }

            return mp3Streams;
        }

        /*public static bool IsEmptyTag(Id3Tag tag)
        {
            bool tagHasData = tag != null &&
                (
                    (tag.Album != null && !string.IsNullOrWhiteSpace(tag.Album.Value)) ||
                    (tag.Title != null && !string.IsNullOrWhiteSpace(tag.Title.Value)) ||
                    (tag.Year != null && tag.Year.Value.HasValue)
                );

            if (tagHasData)
            {
                return false;
            }

            if (tag.Artists != null)
            {
                foreach (string artist in tag.Artists.Value)
                {
                    tagHasData = string.IsNullOrWhiteSpace(artist);
                    if (tagHasData)
                    {
                        return false;
                    }
                }
            }

            return tagHasData == false;
        }

        public static Id3Tag CreateNewTag(string artist, string title, string album, int track, int? year)
        {
            Id3Tag newTag = new Id3Tag();
            newTag.Artists.Value.Add(artist);
            newTag.Title.Value = title;
            newTag.Album.Value = album;
            newTag.Track.Value = Convert.ToInt32(track);
            newTag.Year.Value = Convert.ToInt32(year);
            return newTag;
        }

        public static Id3Tag CreateNewTag(string artist, string title, string album, int track, int? year)
        {
            Id3Tag newTag = new Id3Tag();
            newTag.Artists.Value.Add(artist);
            newTag.Title.Value = title;
            newTag.Album.Value = album;
            newTag.Track.Value = Convert.ToInt32(track);
            newTag.Year.Value = Convert.ToInt32(year);
            return newTag;
        }*/

        public static bool IsEmptyTag(Id3Tag tag)
        {
            bool tagHasData = tag != null &&
                (
                    (tag.Album != null && !string.IsNullOrWhiteSpace(tag.Album.Value)) ||
                    (tag.Title != null && !string.IsNullOrWhiteSpace(tag.Title.Value)) ||
                    (tag.Year != null && !string.IsNullOrWhiteSpace(tag.Year.Value))
                );

            if (tagHasData)
            {
                return false;
            }

            if (tag.Artists != null)
            {
                tagHasData = string.IsNullOrWhiteSpace(tag.Artists.Value);
                if (tagHasData)
                {
                    return false;
                }
            }

            return tagHasData == false;
        }

        public static Id3Tag UpdateTag(Id3Tag existingTag, string artist, string title, string album, string track, string year)
        {
            existingTag.Artists.Value = artist;
            existingTag.Title.Value = title;
            existingTag.Album.Value = album;
            existingTag.Track.Value = track;
            existingTag.Year.Value = year;
            return existingTag;
        }

        public static Id3Tag CreateNewTag(string artist, string title, string album, string track, string year)
        {
            Id3Tag newTag = new Id3Tag();
            newTag.Artists.Value = artist;
            newTag.Title.Value = title;
            newTag.Album.Value = album;
            newTag.Track.Value = track;
            newTag.Year.Value = year;
            return newTag;
        }
    }
}
