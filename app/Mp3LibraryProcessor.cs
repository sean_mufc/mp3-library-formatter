﻿using Id3;
using System;
using System.Collections.Generic;

namespace Mp3LibraryFormatter
{
	public static class Mp3LibraryProcessor
	{
		public static void ProcessLibraryUsingMp3Files(string libraryRootDirectory)
		{
			string[] allArtistFolderPaths = Mp3LibraryManager.GetAllCurrentFolderNames(libraryRootDirectory);

			foreach (string artistFolderPath in allArtistFolderPaths)
			{
				string artistFolderName = Mp3LibraryManager.ExtractNameFromPath(artistFolderPath);
				string[] albumFolderPaths = Mp3LibraryManager.GetAllCurrentFolderNames(artistFolderPath);

				foreach (string albumFolderPath in albumFolderPaths)
				{
					string albumFolderName = Mp3LibraryManager.ExtractNameFromPath(albumFolderPath);
					Dictionary<string, Mp3File> mp3FileDictionary = Mp3LibraryManager.GetAllCurrentMp3SFiles(albumFolderPath);

					foreach (KeyValuePair<string, Mp3File> mp3FileDictonaryEntry in mp3FileDictionary)
					{
						string title = Mp3LibraryManager.ExtractNameFromPath(mp3FileDictonaryEntry.Key);
						title = title.Replace(".mp3", string.Empty);
						title = title.Replace(".MP3", string.Empty);

						Mp3File mp3File = mp3FileDictonaryEntry.Value;

						IEnumerable<Id3Tag> tags = mp3File.GetAllTags();
						bool tagUpdated = false;


						foreach (Id3Tag tag in tags)
						{
							bool isEmptyTag = Mp3LibraryManager.IsEmptyTag(tag);
							if (!isEmptyTag)
							{
								Id3Tag newTag = Mp3LibraryManager.UpdateTag(tag, artistFolderName, title, albumFolderName, tag.Track.Value, tag.Year.Value);
								Console.WriteLine("Completed '{0} - {1}'", newTag.Artists.Value, newTag.Track.Value);
								tagUpdated = true;
							}
						}

						 if(!tagUpdated)
						 {
							 Id3Tag newTag = Mp3LibraryManager.CreateNewTag(artistFolderName, title, albumFolderName, string.Empty, string.Empty);
							 Console.WriteLine("Completed '{0} - {1}'", newTag.Artists.Value, newTag.Track.Value);
							 mp3File.WriteTag(newTag, WriteConflictAction.Replace);
						 }
						 

						mp3File.Dispose();
					}
				}
			}
		}
	
	}
}
