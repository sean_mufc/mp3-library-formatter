﻿using System;
using System.Collections.Generic;

namespace Mp3LibraryFormatter
{
	public static class TagLibProcessor
	{
		public static void ProcessLibraryUsingMp3Files(string libraryRootDirectory)
		{
			string[] allArtistFolderPaths = Mp3LibraryManager.GetAllCurrentFolderNames(libraryRootDirectory);

			foreach (string artistFolderPath in allArtistFolderPaths)
			{
				string artistFolderName = Mp3LibraryManager.ExtractNameFromPath(artistFolderPath);
				string[] albumFolderPaths = Mp3LibraryManager.GetAllCurrentFolderNames(artistFolderPath);

				foreach (string albumFolderPath in albumFolderPaths)
				{
					string albumFolderName = Mp3LibraryManager.ExtractNameFromPath(albumFolderPath);
					Dictionary<string, TagLib.File> mp3FileDictionary = TagLibManager.GetAllCurrentMp3SFiles(albumFolderPath);

					foreach (KeyValuePair<string, TagLib.File> mp3FileDictionaryEntry in mp3FileDictionary)
					{
						ProcessOne(artistFolderName, albumFolderName, mp3FileDictionaryEntry);
					}
				}
			}
		}

		private static void ProcessOne(string artistFolderName, string albumFolderName,  KeyValuePair<string, TagLib.File> mp3FileDictionaryEntry)
		{
			string title = string.Empty;

			try
			{
				title = Mp3LibraryManager.ExtractNameFromPath(mp3FileDictionaryEntry.Key);
				title = title.Replace(".mp3", string.Empty);
				title = title.Replace(".MP3", string.Empty);

				if(title.Contains(" - "))
				{
					string[] splitTitle = title.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
					if(splitTitle.Length == 2)
					{
						artistFolderName = splitTitle[0];
						title = splitTitle[1];
					}
				}

			
				TagLib.File mp3File = mp3FileDictionaryEntry.Value;

				TagLib.Tag tag = mp3File.Tag;
				tag = TagLibManager.UpdateTag(tag, artistFolderName, title, albumFolderName, tag.Track, tag.Year);

				mp3File.Save();
				Console.WriteLine("Completed '{0} - {1}, album {2}'", artistFolderName, title, albumFolderName);
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR '{0} - {1}, album {2}'", artistFolderName, title, albumFolderName);
				Console.WriteLine(ex.ToString());
			}
		}
	
	}
}
